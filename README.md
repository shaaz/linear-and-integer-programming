# README #

Contains all code developed for CAAM571 Spring2016.

* Branch and Bound algorithm for solving Mixed Integer Programs (MIPs)
* Finite Cutting Plane algorithm using Gomory-Chvatal cuts for solving pure IPs.
* Benders' decomposition for solving MIPs

