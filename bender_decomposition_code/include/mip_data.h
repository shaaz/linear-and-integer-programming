/* 
   Spring 2016
   CAAM 571 Assignment: Benders' Decomposition
   Author: Shoeb Mohammed

   Implementation to store MIP data as described in the assignment
*/

#ifndef __MIP_DATA_H__
#define __MIP_DATA_H__

/* data for the MiP 
   MIP formulation should be in standard form.
   That is,
           min (cx + hy)
           s.t.
               Ax + Gy <= b
               x are integral variables
               y are continuous variables
*/
typedef struct {
  double *c;
  int *clen;
  double *h;
  int *hlen;
  
  double *A;
  int *Arows;
  int *Acols;
  double *G;
  int *Grows;
  int *Gcols;

  double *b;
  int *blen;
} MIP_data;

void delete_MIP_data(MIP_data *);

#endif
