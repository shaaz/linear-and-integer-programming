/* 
   Spring 2016
   CAAM 571 Assignment: Benders' Decomposition
   Author: Shoeb Mohammed

   Parse input file as described in the assignment
*/

#ifndef __PARSE_INPUT_H__
#define __PARSE_INPUT_H__

#include "cstd.h"
#include "mip_data.h"

extern void parse_input(const char*, MIP_data*);

#endif
