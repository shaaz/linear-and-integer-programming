/* 
   Spring 2016
   CAAM 571 Assignment: Benders' Decomposition
   Author: Shoeb Mohammed

   Implements Benders' decomposition as a cutting plane algorithm
   uses Gurobi optimization library.
*/


#ifndef __BENDERS_DECOMPOSITION_H__
#define __BENDERS_DECOMPOSITION_H__

#include "mip_data.h"
#include "gurobi_c.h"


typedef enum {BP_NULL, BP_OPTIMAL, BP_INFEASIBLE, BP_UNBOUNDED} BP_status;


/*
  wrapper for Gurobi error reporting.
  The error handler does not return, 
  it exits after reporting error message
*/
void error_handler(GRBenv *);


/* 
   BP is a type to hold Benders' master and sub-problems 
*/
typedef struct {
  GRBmodel *rmp; // relaxed master problem
  GRBmodel *sp; // sub-problem
  MIP_data *data;
  //  MIP *mip; //need decide what to do with it
} BP;



/* 
   constructor for BP
*/
BP* make_benders(GRBenv *, MIP_data * const);



/* 
  copy  constructor for BP
*/
BP* copy_bp(BP *);



/* 
   destructor for BP
*/
void delete_bp(BP *);



/*
  assume Benders sub-problem and master problems are properly initialized. 
*/
int run_benders_decomposition(BP *, double);



/*
  write current master problem solution to file
*/
void benders_write(BP *, const char *);



void update_benders_subproblem_objective(BP *);



/* 
   Add Benders optimality cut.
   This corresponds to extreme point of Benders subproblem.
*/
void add_benders_optimality_cut(BP *);



/* 
   Add Benders feasibility cut.
   This corresponds to extreme ray of Benders subproblem.
*/
void add_benders_feasibility_cut(BP *);

#endif
