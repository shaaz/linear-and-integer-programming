/* 
   Spring 2016
   CAAM 571 Assignment: Benders' Decomposition
   Author: Shoeb Mohammed

   Implementation to store MIP data as described in the assignment
*/

#ifndef __MIP_DATA_C__
#define __MIP_DATA_C__

#include "cstd.h"
#include "mip_data.h"

void delete_MIP_data(MIP_data *data){
  free(data->c);
  free(data->h);
  free(data->A);
  free(data->G);
  free(data->b);
}

#endif
