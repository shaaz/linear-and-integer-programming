/* CAAM 571 Branch and Bound assignment 
   Author: Shoeb Mohammed
   uses Gurobi optimization library to solve linear programs
*/


#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "gurobi_c.h"


/*
   wrapper for GRB error reporting.
   The handler does not return, 
   it exits after reporting error message
*/
void error_handler(GRBenv *env){
	printf("ERROR: %s\n",GRBgeterrormsg(env));
	exit(1);
}

/* 
   LP is a type to store a linear program
*/
typedef struct LP {
	GRBenv   *env;
	GRBmodel *model;
} LP;

LP* make_lp(GRBenv *env, GRBmodel *model){
	LP *lp = NULL;
	lp = malloc(sizeof(LP));
	if(!lp) {
        fprintf(stderr, "ERROR: MAKE_LP: Failed to allocate LP\n");
        exit(1);
	}
	lp -> env = env;
	lp -> model = GRBcopymodel(model);
	
	if(! lp->model ){
        fprintf(stderr, "ERROR: MAKE_LP: Failed to copy model for new LP\n");
        exit(1);		
	}
	return lp;
}

LP* copy_lp(LP *lp){
	LP *copy = NULL;
	copy = malloc(sizeof(LP));
	if(!copy) {
        fprintf(stderr, "ERROR: COPY_LP: Failed to allocate a copy\n");
        exit(1);
	}
	copy -> env = lp->env;
	copy -> model = GRBcopymodel(lp->model);
	
	if(! copy->model ){
        fprintf(stderr, "ERROR: COPY_LP: Failed to copy model\n");
        exit(1);		
	}	
	return copy;
}

void delete_lp(LP *lp){
	if(lp == NULL) return;
	GRBfreemodel(lp->model);
	free(lp);	
}

/*
   qitem is a type to hold items in a queue
*/
typedef struct qitem {
	LP* lp;
	struct qitem *next;
} qitem;

void delete_item(qitem* item){
	if(item == NULL) return;
	free(item);
}


/*
   a Queue is a mutable data structure, with head and tail pointers
*/
typedef struct{
	qitem* head;
	qitem* tail;
} Queue;

Queue* make_empty_queue(){
	Queue *q = NULL;
	q = malloc(sizeof(Queue));
	if(!q) {
        fprintf(stderr, "ERROR: MAKE_EMPTY_QUEUE: Failed to allocated queue\n");
        exit(1);
	}
	q->head = NULL;
	q->tail = NULL;
	return q;
}

/* check if q is empty*/
int is_empty_queue(Queue* q){
	return q->head == NULL;
}

/* insert a linear program at tail of the queue */
void insert_queue(Queue* q,LP* lp){
	qitem *item = NULL;
	item = malloc(sizeof(qitem));
	if(!item) {
        fprintf(stderr, "ERROR: INSERT_QUEUE: Failed to allocate memory for queue item\n");
        exit(1);
	}
	item->lp = lp;
	item->next = NULL;
	
	if(is_empty_queue(q)){
		q->head = item;
		q->tail = item;
	}
	else{
		q->tail->next = item;
		q->tail = item;
	}	
}

/* remove and return first linear program on queue */
LP* get_first_lp_on_queue(Queue* q){
	if(is_empty_queue(q))
		return NULL;
	
	qitem* first_item = q->head;
	LP* lp = first_item->lp;	
	q->head = first_item->next;
	delete_item(first_item);
	
	return lp;
}

/* free memory held by q */
void delete_queue(Queue* q){
	while(!is_empty_queue(q))
		get_first_lp_on_queue(q);
	free(q);
}


/* convert an MIP to a linear program */
void mip2lp(GRBenv* env, GRBmodel* model){
	char vtype;
	int numvars, numintvars;
	int error = 0;
	int *intvars = NULL;
	
   /* Collect integer variables and relax them */
    error = GRBgetintattr(model, "NumVars", &numvars);
    if (error) error_handler(env);
    error = GRBgetintattr(model, "NumIntVars", &numintvars);
    if (error) error_handler(env);
    intvars = malloc(sizeof(int) * numintvars);
    if (!intvars) error_handler(env);
	
    for (int j = 0; j < numvars; j++){
      error = GRBgetcharattrelement(model, "VType", j, &vtype);
      if (error) error_handler(env);
      if (vtype != GRB_CONTINUOUS){
        error = GRBsetcharattrelement(model, "VType", j, GRB_CONTINUOUS);
        if (error) error_handler(env);
      }
    }
	error = GRBupdatemodel(model);
	if (error) error_handler(env);
	free(intvars);
}


/* check if solution to linear program is all integers */
int is_integral_solution(LP *lp){
	char vtype;
	int numvars;
	int error = 0;
	double x;

    error = GRBgetintattr(lp->model, "NumVars", &numvars);
    if (error) error_handler(lp->env);
	
    for (int j = 0; j < numvars; j++){
      error = GRBgetcharattrelement(lp->model, "VType", j, &vtype);
      if (error) error_handler(lp->env);
	  
      if (vtype == GRB_CONTINUOUS) {
		  error = GRBgetdblattrelement(lp->model, "X", j, &x);
		  if (error) error_handler(lp->env);			  	  	  
		  if(floor(x)!=x) return 0; //return false  
      }	  
    }

	return 1; //return true
}

/* divide the linear program into two sub-problems
   and put them on queue. We find the first variable
   that is not an integer and create sub-problems
   by adding constraints for that variable
*/
int find_non_integervar(LP *lp, int *intvars, int numintvars){
	double x;
	int error = 0;
    for (int j = 0; j < numintvars; j++){
	  	error = GRBgetdblattrelement(lp->model, "X", intvars[j], &x);
		if (error) error_handler(lp->env);		  
		if(floor(x)!=x) return intvars[j];
      }
	return -1; //error
}

void add_constraint_to_sub_problem(LP *sublp, int index){
	int error = 0;
	double x;
	double coeff = 1.0;	
	
  	error = GRBgetdblattrelement(sublp->model, "X", index, &x);
	if (error) error_handler(sublp->env);
	
	error = GRBaddconstr(sublp->model,1,&index,&coeff,GRB_LESS_EQUAL,floor(x),NULL);
	if (error) error_handler(sublp->env);
	
	error = GRBaddconstr(sublp->model,1,&index,&coeff,GRB_GREATER_EQUAL,ceil(x),NULL);
	if (error) error_handler(sublp->env);
	
	error = GRBupdatemodel(sublp->model);
	if (error) error_handler(sublp->env);
}

void add_subproblems_to_queue(Queue* q, LP *lp, int *intvars, int numintvars){
	int i = find_non_integervar(lp, intvars, numintvars);	
	
	int error = 0;
	
	LP *sublp1 = make_lp(lp->env,lp->model);
	LP *sublp2 = make_lp(lp->env,lp->model);
		
	add_constraint_to_sub_problem(sublp1,i);
	add_constraint_to_sub_problem(sublp2,i);
	
    insert_queue(q,sublp1);
	insert_queue(q,sublp2);		
}


int main(int   argc,
     char *argv[]){
	
    if (argc < 2) {
       fprintf(stderr, "Usage: b_b2.x filename\n");
       exit(1);
     }
	 
	GRBenv	*env = NULL;
	GRBmodel *model = NULL;
	int error = 0;
	int ismip = 0;
	int optimstatus;
	double objval;
	double best_objval;
	
	/* read in MIP from mps file */
	error = GRBloadenv(&env,"b_b2.log");
	if(error)	error_handler(env);

    error = GRBreadmodel(env, argv[1], &model);
	if(error)	error_handler(env);
	
    error = GRBgetintattr(model, "IsMIP", &ismip);
	if(error)	error_handler(env);
	
    if (ismip == 0) {
       printf("Model is not a MIP\n");
       error_handler(env);
     }
	 
	 
	 
 	char vtype;
 	int numvars, numintvars;
	int *intvars = NULL;
		
    error = GRBgetintattr(model, "NumVars", &numvars);
    if (error) error_handler(env);
    error = GRBgetintattr(model, "NumIntVars", &numintvars);
    if (error) error_handler(env);
    intvars = malloc(sizeof(int) * numintvars);
    if (!intvars) error_handler(env);
	
	int index=0;
    for (int j = 0; j < numvars; j++){
      error = GRBgetcharattrelement(model, "VType", j, &vtype);
      if (error) error_handler(env);
      if (vtype != GRB_CONTINUOUS)
	   intvars[index++] = j;
     }
	 
	 
	/* convert MIP to linear program 
	    by relaxing the integrality conditions
	*/
	mip2lp(env,model);
	
	
	/* Initialization */
	Queue *q = make_empty_queue();	
	best_objval = 1.0/0.0;
	LP *lp = make_lp(env,model);
	insert_queue(q,lp);
	LP *best_lp = NULL;
	
	
	/* Branch and Bound */
	while(! is_empty_queue(q)){
		/* get first sub problem on the queue */	
		LP* lp = get_first_lp_on_queue(q);
		
	    /* Solve sub-problem */
	    error = GRBoptimize(lp->model);
		if(error)	error_handler(lp->env);
		
		
	    /* Capture solution information */
	    error = GRBgetintattr(lp->model, GRB_INT_ATTR_STATUS, &optimstatus);
	    if (error) error_handler(lp->env);

	    if (optimstatus == GRB_OPTIMAL) {
	      error = GRBgetdblattr(lp->model, GRB_DBL_ATTR_OBJVAL, &objval);
  	  	  if (error) error_handler(env);
	    } else if (optimstatus == GRB_INF_OR_UNBD) {
	      continue;
	    } else if (optimstatus == GRB_INFEASIBLE) {
		  continue;
	    } else if (optimstatus == GRB_UNBOUNDED) {
  		  continue;
	    } else {
	      printf("Optimization was stopped with status = %d\n\n", optimstatus);
	  	  if(error)	error_handler(env);		
	    }
		
		/* Current sub-problem objective is not better than the best value so far,
		   so try next sub-problem on queue */
		if(objval >= best_objval) continue;		
		
		/* Current sub-problem objective is better than the best value so far,
		   and the solution is integral, so update the best solution
		*/
		if(is_integral_solution(lp)){
			best_objval = objval;
			best_lp = copy_lp(lp);
			continue;
		}		
		
		/* Current sub-problem has no integral solution but further sub-problems
		   need to be explored */
		add_subproblems_to_queue(q,lp,intvars,numintvars);
				
		/* free the LP memory */
		delete_lp(lp);
    }
	
	/* optimization was unsuccessful, report error and exit */
	if(best_objval == 1.0/0.0)	error_handler(env);
	
	/* print the results to solution.mps */
	error = GRBwrite(best_lp->model,"./optimal.sol.gz");
	if (error) error_handler(env);
	
	delete_queue(q);
	delete_lp(best_lp);
	GRBfreeenv(env);
	
	return 0;
}