/* 
   Spring 2016
   CAAM 571 Assignment: Benders' Decomposition
   Author: Shoeb Mohammed

   Parse input file as described in the assignment
*/

#ifndef __PARSE_INPUT_CPP__
#define __PARSE_INPUT_CPP__

#include "cppstd.hh"
#include "mip_data.h"

extern "C" void parse_input(const char* file, MIP_data* data){
  ifstream in(file);
  string line;

  vector<double> data_items;
  string x;

  while(getline(in,line)){
    istringstream iss(line);
    while(iss >> x)
      data_items.push_back(stod(x));
  }
  in.close();
  
  int facilities = (int) data_items[0];
  int customers = (int) data_items[1];
  *(data->clen) = facilities;
  *(data->hlen) = facilities * customers;
  *(data->blen) = facilities + customers;
  *(data->Arows) = facilities + customers;
  *(data->Acols) = facilities;
  *(data->Grows) = facilities + customers;
  *(data->Gcols) = facilities * customers ;

  /* allocate memory for data */
  data->b = (double *) malloc(*(data->blen) * sizeof(double));
  if(!data->b) {
    fprintf(stderr, "ERROR: PARSE_INPUT: malloc failed to allocate data->b\n");
    exit(1);
  }
  data->c = (double *) malloc(*(data->clen) * sizeof(double));
  if(!data->c) {
    fprintf(stderr, "ERROR: PARSE_INPUT: malloc failed to allocate data->c\n");
    exit(1);
  }  
  data->h = (double *) malloc(*(data->hlen) * sizeof(double));
  if(!data->h) {
    fprintf(stderr, "ERROR: PARSE_INPUT: malloc failed to allocate data->h\n");
    exit(1);
  }  
  data->A = (double *) malloc( (*(data->Arows)) * (*(data->Acols))  * sizeof(double));
  if(!data->A) {
    fprintf(stderr, "ERROR: PARSE_INPUT: malloc failed to allocate data->A\n");
    exit(1);
  }  
  data->G = (double *) malloc( (*(data->Grows)) * (*(data->Gcols))  * sizeof(double));
  if(!data->G) {
    fprintf(stderr, "ERROR: PARSE_INPUT: malloc failed to allocate data->G\n");
    exit(1);
  }  

  /* store array c */
  for(int i=0; i<*(data->clen);i++)
    data->c[i] = data_items[2+customers+facilities+i];
  
  /* store array h */
  for(int i=0; i<*(data->hlen);i++)
    data->h[i] = data_items[2+customers+2*facilities+i];

  /* store array b */
  for(int i=0; i<facilities;i++)
    data->b[i] = 0;
  for(int i=0; i<customers;i++)
    data->b[facilities+i] = -1.0*data_items[2+i];;

  /* store array A */
  for(int i=0; i<facilities;i++)
    for(int j=0; j<*(data->Acols);j++)
      data->A[(i* *(data->Acols)) + j] = ( i==j ? -1.0*data_items[2+customers+i] : 0.0);
  for(int i=facilities; i<facilities+customers;i++)
    for(int j=0; j<*(data->Acols);j++)
      data->A[(i* *(data->Acols)) + j] = 0.0;

  /* store array G */
  for(int i=0; i<facilities;i++)
    for(int j=0; j<*(data->Gcols);j++)
      data->G[(i* *(data->Gcols)) + j] = (j == (i-1)*customers + (j%customers -1))  ? 1.0 : 0.0;
  for(int i=facilities; i<facilities+customers;i++)
    for(int j=0; j<*(data->Gcols);j++)
      data->G[(i* *(data->Gcols)) + j] = (j == (j%customers -1)*customers + (i-facilities -1))  ? -1.0 : 0.0;      
}

#endif
