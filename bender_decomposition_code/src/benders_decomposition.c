/* 
   Spring 2016
   CAAM 571 Assignment: Benders' Decomposition
   Author: Shoeb Mohammed

   Implements Benders' decomposition as a cutting plane algorithm
   uses Gurobi optimization library.
*/


#ifndef __BENDERS_DECOMPOSITION_C__
#define __BENDERS_DECOMPOSITION_C__

#include "cstd.h"
#include "mip_data.h"
#include "benders_decomposition.h"

void error_handler(GRBenv *env){
  fprintf(stderr, "ERROR: %s\n",GRBgeterrormsg(env));
  exit(1);
}

/* 
   constructor for BP
*/
BP* make_benders(GRBenv *env, MIP_data * const data){
  int error = 0;  
  BP *bp = NULL;
  bp =(BP *)  malloc(sizeof(BP));
  if(!bp) {
    fprintf(stderr, "ERROR: MAKE_BENDERS: malloc failed to allocate BP\n");
    exit(1);
  }

  bp->data = data;
  
  /* initialize Benders relaxed master problem
     master problem is initialized with no constrainsts
  */
  char * rmp_vtype = (char *) malloc(*(data->clen)  * sizeof(char));
  if(!rmp_vtype) {
    fprintf(stderr, "ERROR: MAKE_BENDERS: malloc failed to allocate rmp_vtype\n");
    exit(1);
  }
  for(int i=0; i < *(data->clen); i++)
    rmp_vtype[i]=GRB_BINARY;

  error = GRBnewmodel(env,&(bp->rmp),"Benders_relaxed_master_problem",*(data->clen),data->c,NULL,NULL,rmp_vtype,NULL);
  if(error) error_handler(env);
  GRBenv *rmp_env = GRBgetenv(bp->rmp);
  if (!rmp_env) fprintf(stderr, "ERROR: MAKE_BENDERS: Failed to get env from benders master problem\n");  
  error = GRBaddvar(bp->rmp,0,NULL,NULL,1.0,0.0,GRB_INFINITY,GRB_CONTINUOUS,"theta");
  if(error) error_handler(rmp_env);
  error = GRBsetintattr(bp->rmp,"ModelSense",-1);
  if(error) error_handler(rmp_env);
  error = GRBsetintparam(rmp_env,"Presolve",0);
  if(error) error_handler(rmp_env);
  error = GRBupdatemodel(bp->rmp);
  if(error) error_handler(rmp_env);  
  
    
  /* initialize Benders sub-problem 
     For initialization we choose x=0 (which determines objective of the sub-problem)
  */
  int sb_nvars = *(data->Grows);
  int sb_nconstrs = *(data->Gcols);
  char * sb_constraint_sense = (char *) malloc(sb_nconstrs*sizeof(char));
  if(!sb_constraint_sense) {
    fprintf(stderr, "ERROR: MAKE_BENDERS: malloc failed to allocate sb_constraint_sense\n");
    exit(1);
  }
  for(int i=0; i < sb_nconstrs; i++)
    sb_constraint_sense[i]=GRB_GREATER_EQUAL;

  int *sb_vbeg = (int *) malloc (sb_nvars*sizeof(int));
  if(!sb_vbeg) {
    fprintf(stderr, "ERROR: MAKE_BENDERS: malloc failed to allocate sb_vbeg\n");
    exit(1);
  }
  for(int i=0; i < sb_nvars; i++)
    sb_vbeg[i]=i*sb_nconstrs;
  
  int *sb_vlen = (int *) malloc (sb_nvars*sizeof(int));
  if(!sb_vlen) {
    fprintf(stderr, "ERROR: MAKE_BENDERS: malloc failed to allocate sb_vlen\n");
    exit(1);
  }
  for(int i=0; i < sb_nvars; i++)
    sb_vlen[i]=i*sb_nconstrs;  
  
  int *sb_vind = (int *) malloc (sb_nvars*sb_nconstrs*sizeof(int));
  if(!sb_vind) {
    fprintf(stderr, "ERROR: MAKE_BENDERS: malloc failed to allocate sb_vind\n");
    exit(1);
  }
  for(int i=0; i < sb_nvars; i++)
    for(int j=0; j < sb_nconstrs; j++)
      sb_vind[i*sb_nconstrs + j]= j;
  
  double *sb_vval = (double *) malloc (sb_nvars*sb_nconstrs*sizeof(double));
  if(!sb_vval) {
    fprintf(stderr, "ERROR: MAKE_BENDERS: malloc failed to allocate sb_vval\n");
    exit(1);
  }      
  for(int i=0; i < sb_nvars; i++)
    for(int j=0; j < sb_nconstrs; j++)
      sb_vval[i*sb_nconstrs + j]= (data->G)[j*sb_nvars + i];
	
  error = GRBloadmodel(env,&(bp->sp),"Benders_sub_problem",sb_nvars,sb_nconstrs,1,0.0,data->b,sb_constraint_sense,data->h,sb_vbeg,sb_vlen,sb_vind,sb_vval,NULL,NULL,NULL,NULL,NULL);
  if(error) error_handler(env);
  GRBenv *sp_env = GRBgetenv(bp->sp);
  if (!sp_env) fprintf(stderr, "ERROR: MAKE_BENDERS: Failed to get env from benders sub-problem\n");    
  error = GRBsetintparam(sp_env,"InfUnbdInfo",1);
  if(error) error_handler(sp_env);
  error = GRBsetintparam(sp_env,"Presolve",0);
  if(error) error_handler(sp_env);
  error = GRBupdatemodel(bp->sp);
  if(error) error_handler(sp_env);

  return bp;
}

/* 
  copy  constructor for BP
*/
BP* copy_bp(BP *bp){
  BP *copy = NULL;
  copy = (BP *) malloc(sizeof(BP));
  if(!copy) {
    fprintf(stderr, "ERROR: COPY_BP: malloc failed to allocate BP\n");
    exit(1);
  }

  copy->data = bp->data;
  copy->rmp = GRBcopymodel(bp->rmp);
  copy->sp = GRBcopymodel(bp->sp);
        
  if(! (copy->rmp || copy->sp) ){
    fprintf(stderr, "ERROR: COPY_BP: Failed to make new copy\n");
    exit(1);                
  }

  return copy;
}

/* 
   destructor for BP
*/
void delete_bp(BP *bp){
   if(bp == NULL) return;
   GRBenv *rmp_env = GRBgetenv(bp->rmp);
   if (!rmp_env) fprintf(stderr, "ERROR: DELETE_BP: Failed to get env from benders master problem\n");   
   GRBenv *sb_env = GRBgetenv(bp->sp);
   if (!sb_env) fprintf(stderr, "ERROR: DELETE_BP: Failed to get env from benders sub-problem\n");   
   GRBfreemodel(bp->rmp);
   GRBfreemodel(bp->sp);
   GRBfreeenv(rmp_env);
   GRBfreeenv(sb_env);
   free(bp);       
}

/*
  assume Benders sub-problem and master problems are properly initialized. 
*/
int run_benders_decomposition(BP *bp, double tol){
  int error = 0;
  BP_status bp_status = BP_NULL;
  int optimstatus = 0;
  double objval = -1.0/0.0;
  double UB = 1.0/0.0;
  double LB = -1.0/0.0;
  double z = 0;
  int a = 0;

  GRBenv *rmp_env = GRBgetenv(bp->rmp);
  if (!rmp_env) fprintf(stderr, "ERROR: RUN_BENDERS_DECOMPOSITION: Failed to get env from benders master problem\n");          
  GRBenv *sp_env = GRBgetenv(bp->sp);
  if (!sp_env) fprintf(stderr, "ERROR: RUN_BENDERS_DECOMPOSITION: Failed to get env from benders sub-problem\n");      
  
  while(1){
    /* solve benders sub-problem */
    error = GRBoptimize(bp->sp);
    if(error) error_handler(sp_env);
    error = GRBgetintattr(bp->sp,GRB_INT_ATTR_STATUS, &optimstatus);
    if(error) error_handler(sp_env);

    if(optimstatus == GRB_OPTIMAL){
      error = GRBgetdblattr(bp->sp,"ObjVal",&objval);
      if(error) error_handler(sp_env);
      error = GRBgetdblattr(bp->rmp,"ObjVal",&z);
      if(error) error_handler(rmp_env);
      objval += z;

      error = GRBgetvarbyname(bp->rmp,"theta",&a);
      if(error) error_handler(rmp_env);
      error = GRBgetdblattrelement(bp->rmp,"X",a,&z);
      if(error) error_handler(rmp_env);
      objval -= z;

      LB = (LB > objval) ? LB : objval;
      if(UB-LB < tol){
	bp_status = BP_OPTIMAL;
	break;
      }
      add_benders_optimality_cut(bp);      
    }else if(optimstatus == GRB_INFEASIBLE){
      bp_status = BP_UNBOUNDED;
      break;
    }else if(optimstatus == GRB_UNBOUNDED){
      add_benders_feasibility_cut(bp);
    }else{
      bp_status = (BP_status) optimstatus;
      fprintf(stderr,"ERROR: RUN_BENDERS_DECOMPOSITION: sub-problem optimization was stopped with status = %d\n",optimstatus);
    }

    /* solve benders master problem */
    error = GRBoptimize(bp->rmp);
    if(error) error_handler(rmp_env);
    
    if(optimstatus == GRB_OPTIMAL){
      error = GRBgetdblattr(bp->rmp,"ObjVal",&UB);
      if(error) error_handler(rmp_env); 
      if(UB-LB <= tol){
	bp_status = BP_OPTIMAL;
	break;
      }
      update_benders_subproblem_objective(bp);      
    }else
      fprintf(stderr,"ERROR: RUN_BENDERS_DECOMPOSITION: master problem optimization was stopped with status = %d\n",optimstatus);
  }

  return bp_status;
}


/*
  write current master problem solution to file
*/
void benders_write(BP *bp, const char *file){
  GRBenv *rmp_env = GRBgetenv(bp->rmp);
  if (!rmp_env) fprintf(stderr, "ERROR: BENDERS_WRITE: Failed to get env from benders master problem\n");      
  
  int error = 0;
  error = GRBwrite(bp->rmp,file);
  if(error) error_handler(rmp_env);
}


void update_benders_subproblem_objective(BP *bp){
  int numvars_x, numconstrs_A;	
  int error = 0;
	
  GRBenv* sp_env = GRBgetenv(bp->sp);
  if (!sp_env) fprintf(stderr, "ERROR: UPDATE_BENDERS_SUBPROBLEM_OBJECTIVE: Failed to get env from benders sub-problem\n");
  GRBenv* rmp_env = GRBgetenv(bp->rmp);
  if (!rmp_env) fprintf(stderr, "ERROR: UPDATE_BENDERS_SUBPROBLEM_OBJECTIVE: Failed to get env from benders master problem\n");  
	
  numvars_x = *(bp->data->clen);
  numconstrs_A = *(bp->data->Arows);

  /* get the current solution from master problem */
  double *x = (double *) malloc( sizeof(double) * (numvars_x));
  if (!x) fprintf(stderr, "ERROR: UPDATE_BENDERS_SUBPROBLEM_OBJECTIVE: malloc failed to allocate x\n");
  for(int i=0; i<numvars_x;i++){
    error = GRBgetdblattrelement(bp->rmp,"X",i,&x[i]);
    if(error) error_handler(rmp_env);
  }

  /* calculate rhs for the new benders cut: vb, where v is extreme point from sub-problem */
  double rhs = 0;
  for(int i=0; i<numconstrs_A; i++)
    rhs += x[i] * bp->data->b[i];

  /* calculate lhs for new benders cut : vA, where v is extreme ray from sub-problem */
  double *val = (double *) malloc( sizeof(double) * numconstrs_A);
  if (!val) fprintf(stderr, "ERROR: UPDATE_BENDERS_SUBPROBLEM_OBJECTIVE: malloc failed to allocate val\n");  
  for (int i=0; i < numconstrs_A; i++){
    val[i] = 0;
    for (int j=0; j<numvars_x; j++)
      val[i] += x[j]*bp->data->A[(i*numvars_x)+j];
  }
  for(int i=0; i<numconstrs_A;i++)
    val[i] = bp->data->b[i] - val[i];
  
  int numnz = 0;
  for (int i=0; i < numvars_x; i++){
    if(val[i] != 0) numnz++;
  }
  int *cind = (int *) malloc( sizeof(int) * numnz);
  if (!cind) fprintf(stderr, "ERROR: UPDATE_BENDERS_SUBPROBLEM_OBJECTIVE: malloc failed to allocate cind\n");    
  double *cval = (double *) malloc( sizeof(double) * numnz);
  if (!cval) fprintf(stderr, "ERROR: UPDATE_BENDERS_SUBPROBLEM_OBJECTIVE: malloc failed to allocate cval\n");
  for (int i=0, j=0; i < numvars_x; i++)
    if(val[i] != 0) {
      cind[j] = i;
      cval[j] = val[i];
      j++;
    }
  
  /* update Benders sub-problem objective */
  error = GRBaddconstr(bp->rmp,numnz,cind,cval,GRB_LESS_EQUAL,rhs,NULL);
  if (error) error_handler(rmp_env);
	
  error = GRBupdatemodel(bp->rmp);
  if (error) error_handler(rmp_env);
	
  /* free heap memory */
  free(cval);
  free(cind);    
  free(val);
}

/* 
   Add Benders optimality cut.
   This corresponds to extreme point of Benders subproblem.
*/
void add_benders_optimality_cut(BP *bp){
  int numvars_x, numconstrs_A;	
  int error = 0;
	
  GRBenv* sp_env = GRBgetenv(bp->sp);
  if (!sp_env) fprintf(stderr, "ERROR: ADD_BENDERS_OPTIMALITY_CUT: Failed to get env from benders sub-problem\n");
  GRBenv* rmp_env = GRBgetenv(bp->rmp);
  if (!rmp_env) fprintf(stderr, "ERROR: ADD_BENDERS_OPTIMALITY_CUT: Failed to get env from benders master problem\n");  
	
  numvars_x = *(bp->data->clen);
  numconstrs_A = *(bp->data->Arows);

  /* get the current extreme point solution from sub-problem */
  double *v = (double *) malloc( sizeof(double) * (numconstrs_A));
  if (!v) fprintf(stderr, "ERROR: ADD_BENDERS_OPTIMALITY_CUT: malloc failed to allocate v\n");
  for(int i=0; i<numconstrs_A;i++){
    error = GRBgetdblattrelement(bp->sp,"X",i,&v[i]);
    if(error) error_handler(sp_env);
  }
  
  /* calculate rhs for the new benders cut: vb, where v is extreme point from sub-problem */
  double rhs = 0;
  for(int i=0; i<numconstrs_A; i++)
    rhs += v[i] * bp->data->b[i];

  /* calculate lhs for new benders cut : vA, where v is extreme point from sub-problem */
  double *val = (double *) malloc( sizeof(double) * (numvars_x+1));
  if (!val) fprintf(stderr, "ERROR: ADD_BENDERS_OPTIMALITY_CUT: malloc failed to allocate val\n");  
  for (int i=0; i < numvars_x; i++){
    val[i] = 0;
    for (int j=0; j<numconstrs_A; j++)
      val[i] += v[j]*bp->data->A[(j*numvars_x)+i];
  }
  val[numvars_x+1] = 1.0;
  
  int numnz = 0;
  for (int i=0; i < numvars_x; i++){
    if(val[i] != 0) numnz++;
  }
  int *cind = (int *) malloc( sizeof(int) * numnz);
  if (!cind) fprintf(stderr, "ERROR: ADD_BENDERS_OPTIMALITY_CUT: malloc failed to allocate cind\n");    
  double *cval = (double *) malloc( sizeof(double) * numnz);
  if (!cval) fprintf(stderr, "ERROR: ADD_BENDERS_OPTIMALITY_CUT: malloc failed to allocate cval\n");
  for (int i=0, j=0; i < numvars_x+1; i++)
    if(val[i] != 0) {
      cind[j] = i;
      cval[j] = val[i];
      j++;
    }
  
  /* add Benders optimality cut */
  error = GRBaddconstr(bp->rmp,numnz,cind,cval,GRB_LESS_EQUAL,rhs,NULL);
  if (error) error_handler(rmp_env);
	
  error = GRBupdatemodel(bp->rmp);
  if (error) error_handler(rmp_env);
	
  /* free heap memory */
  free(cval);
  free(cind);    
  free(val);
}
  
/* 
   Add Benders feasibility cut.
   This corresponds to extreme ray of Benders subproblem.
*/
void add_benders_feasibility_cut(BP *bp){
  int numvars_x, numconstrs_A;	
  int error = 0;
	
  GRBenv* sp_env = GRBgetenv(bp->sp);
  if (!sp_env) fprintf(stderr, "ERROR: ADD_BENDERS_FEASIBILITY_CUT: Failed to get env from benders sub-problem\n");
  GRBenv* rmp_env = GRBgetenv(bp->rmp);
  if (!rmp_env) fprintf(stderr, "ERROR: ADD_BENDERS_FEASIBILITY_CUT: Failed to get env from benders master problem\n");  
	
  numvars_x = *(bp->data->clen);
  numconstrs_A = *(bp->data->Arows);


  /* get the current extreme point solution from sub-problem */
  double *v = (double *) malloc( sizeof(double) * (numconstrs_A));
  if (!v) fprintf(stderr, "ERROR: ADD_BENDERS_FEASIBILITY_CUT: malloc failed to allocate v\n");
  for(int i=0; i<numconstrs_A;i++){
    error = GRBgetdblattrelement(bp->sp,"UnbdRay",i,&v[i]);
    if(error) error_handler(sp_env);
  }
  
  
  /* calculate rhs for the new benders cut: vb, where v is extreme ray from sub-problem */
  double rhs = 0;
  for(int i=0; i<numconstrs_A; i++)
    rhs += v[i] * bp->data->b[i];

  /* calculate lhs for new benders cut : vA, where v is extreme ray from sub-problem */
  double *val = (double *) malloc( sizeof(double) * numvars_x);
  if (!val) fprintf(stderr, "ERROR: ADD_BENDERS_FEASIBILITY_CUT: malloc failed to allocate val\n");  
  for (int i=0; i < numvars_x; i++){
    val[i] = 0;
    for (int j=0; j<numconstrs_A; j++)
      val[i] += v[j]*bp->data->A[(j*numvars_x)+i];
  }
  
  int numnz = 0;
  for (int i=0; i < numvars_x; i++){
    if(val[i] != 0) numnz++;
  }
  int *cind = (int *) malloc( sizeof(int) * numnz);
  if (!cind) fprintf(stderr, "ERROR: ADD_BENDERS_FEASIBILITY_CUT: malloc failed to allocate cind\n");    
  double *cval = (double *) malloc( sizeof(double) * numnz);
  if (!cval) fprintf(stderr, "ERROR: ADD_BENDERS_FEASIBILITY_CUT: malloc failed to allocate cval\n");
  for (int i=0, j=0; i < numvars_x; i++)
    if(val[i] != 0) {
      cind[j] = i;
      cval[j] = val[i];
      j++;
    }
  
  /* add Benders feasibility cut */
  error = GRBaddconstr(bp->rmp,numnz,cind,cval,GRB_LESS_EQUAL,rhs,NULL);
  if (error) error_handler(rmp_env);
	
  error = GRBupdatemodel(bp->rmp);
  if (error) error_handler(rmp_env);
	
  /* free heap memory */
  free(cval);
  free(cind);    
  free(val);
}	


#endif
