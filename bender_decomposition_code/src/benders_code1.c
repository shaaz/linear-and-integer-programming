/* 
   Spring 2016
   CAAM 571 Assignment: Benders' Decomposition
   Author: Shoeb Mohammed
*/

#include "parse_input.h"
#include "benders_decomposition.h"

int main(int argc, char *argv[]){
  if (argc < 2) {
    fprintf(stderr, "Usage: benders_code1.x filename\n");
    exit(1);
  }
	 
  MIP_data data;
  parse_input(argv[1],&data);
  
  GRBenv   *env = NULL;
  int error = 0;
  error = GRBloadenv(&env,"benders_code1.log");
  if(error) error_handler(env);
  
  BP* bp = make_benders(env,&data);
  
  int bp_status=BP_NULL;  
  bp_status = run_benders_decomposition(bp,1e-5);
		  
  if (bp_status != BP_OPTIMAL){
    printf("Benders decomposition was stopped with status = %d\n\n", bp_status);
	return 1;
  }	
  	
  /* print the results to the file benders_code1.sol */
  benders_write(bp,"./benders_code1_optimal.sol");
	
  /* free memory */
  delete_bp(bp);
  delete_MIP_data(&data);
  GRBfreeenv(env);
	
  return 0;
}
