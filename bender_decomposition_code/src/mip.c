/* CAAM 571 

   Author: Shoeb Mohammed
   uses Gurobi optimization library
*/


#ifndef __MIP__
#define __MIP__

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "gurobi_c.h"


/*
  wrapper for GRB error reporting.
  The handler does not return, 
  it exits after reporting error message
*/
void error_handler(GRBenv * const env){
  printf("ERROR: %s\n",GRBgeterrormsg(env));
  exit(1);
}

typedef GRBmodel *model MIP;

/* 
   constructor for MIP 
   note: MIP gets the same environment as model
*/
MIP* make_mip(GRBmodel * const model){
  MIP *mip = NULL;
  mip = malloc(sizeof(MIP));
  if(!mip) {
    fprintf(stderr, "ERROR: MAKE_MIP: malloc failed to allocate from heap\n");
    exit(1);
  }

  *mip = GRBcopymodel(model);
  
  if(! *mip ){
    fprintf(stderr, "ERROR: MAKE_MIP: Failed to make a new MIP\n");
    exit(1);		
  }
  return mip;
}


/* 
   copy constructor for MIP 
   note: the new copy gets the same environment as the original
*/
MIP* copy_mip(MIP * const mip){
  MIP *copy = NULL;
  copy = malloc(sizeof(MIP));
  if(!copy) {
    fprintf(stderr, "ERROR: COPY_MIP: malloc failed to allocate from heap\n");
    exit(1);
  }

  *copy = GRBcopymodel(*mip);
	
  if(! copy->model ){
    fprintf(stderr, "ERROR: COPY_MIP: Failed to copy model\n");
    exit(1);		
  }	
  return copy;
}

/* 
   destructor for MIP 
*/
void delete_mip(MIP * const mip){
  if(mip == NULL) return;
  GRBfreemodel(*mip);
  free(mip);	
}


#endif
