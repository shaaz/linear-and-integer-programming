1. Source code in file cg_procedure.c

2. A simple Makefile is provided. 
   Before compiling the makefile should be edited. 
   In particular the $(INC) and $(CLIB) variables should correspond Gurobi include/library paths on the machine.
   After successful make, an executable cg_procedure.x is generated.

3. Three test cases for the knapsack are included. These are test1.mps , test2.mps, and test3.mps
   To run a problem instance, type following at command prompt
   ./cg_procedure.x <file_name>
   
4. The optimum solution (if successful) will be written to a file 'optimal.sol' in present working directory.   