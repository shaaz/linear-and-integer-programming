NAME          knapsack2
ROWS
 L  R0001
 N  R0002
COLUMNS
    INT1      'MARKER'                 'INTORG'
    C0001     R0001     4
    C0001     R0002     10
    C0002     R0001     5
    C0002     R0002     12
    C0003     R0001     3
    C0003     R0002     7
    C0004     R0001     1
    C0004     R0002     1.5
    INT1END   'MARKER'                 'INTEND'
RHS
    B         R0001     10
BOUNDS
 LO BOUND     C0001     0
 LO BOUND     C0002     0
 LO BOUND     C0003     0
 LO BOUND     C0004     0
 UP BOUND     C0003     1
 UP BOUND     C0004     1
ENDATA