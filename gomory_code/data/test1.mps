NAME          knapsack1
ROWS
 L  R0001
 N  R0002
COLUMNS
    INT1      'MARKER'                 'INTORG'
    C0001     R0001     35
    C0001     R0002     5
    C0002     R0001     23
    C0002     R0002     3
    C0003     R0001     17
    C0003     R0002     2
    INT1END   'MARKER'                 'INTEND'
RHS
    B         R0001     1195
BOUNDS
 LO BOUND     C0001     0
 LO BOUND     C0002     0
 LO BOUND     C0003     0
ENDATA