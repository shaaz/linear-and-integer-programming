/* 
   Spring 2016
   CAAM 571 Assignment: Chv ́atal-Gomory Procedure
   use Gurobi optimization library.

   Author: Shoeb Mohammed

   The implementation solves any pure IP; it is not limited to integer knapsack problems.

   The input is assumed to be a pure IP (no error checking done.)
		* All constraints should be specified in 'less than equal' sense.
		* The input should be in any of the formats Gurobi natively supports 
          (didn't implement the file format described in assignment yet).

   We use the Gurobi solver to solve LP relaxation of pure IP.
*/


#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "gurobi_c.h"


/*
   wrapper for Gurobi error reporting.
   The error handler does not return, 
   it exits after reporting error message
*/
void error_handler(GRBenv *env){
	fprintf(stderr, "ERROR: %s\n",GRBgeterrormsg(env));
	exit(1);
}


/* relax an IP to LP */
void ip2lp(GRBmodel* model){
	char vtype;
	int numvars, numintvars;
	int error = 0;
	int *intvars = NULL;
	
	GRBenv* env = GRBgetenv(model);
	if (!env) fprintf(stderr, "ERROR: IP2LP: Failed to get env from Gurobi model\n");
	
   /* Collect integer variables and relax them */
    error = GRBgetintattr(model, "NumVars", &numvars);
    if (error) error_handler(env);
    error = GRBgetintattr(model, "NumIntVars", &numintvars);
    if (error) error_handler(env);
	
    for (int j = 0; j < numvars; j++){
      error = GRBgetcharattrelement(model, "VType", j, &vtype);
      if (error) error_handler(env);
      if (vtype != GRB_CONTINUOUS){
        error = GRBsetcharattrelement(model, "VType", j, GRB_CONTINUOUS);
        if (error) error_handler(env);
      }
    }
	error = GRBupdatemodel(model);
	if (error) error_handler(env);
}


/* check if solution to LP relaxation is all integers */
int is_integral_solution(GRBmodel* model){
	char vtype;
	int numvars;
	int error = 0;
	double x;
	
	GRBenv* env = GRBgetenv(model);
	if (!env) fprintf(stderr, "ERROR: IS_INTEGRAL_SOLUTION: Failed to get env from Gurobi model\n");

    error = GRBgetintattr(model, "NumVars", &numvars);
    if (error) error_handler(env);
	
    for (int j = 0; j < numvars; j++){
      error = GRBgetcharattrelement(model, "VType", j, &vtype);
      if (error) error_handler(env);
	  
      if (vtype == GRB_CONTINUOUS) {
		  error = GRBgetdblattrelement(model, "X", j, &x);
		  if (error) error_handler(env);			  	  	  
		  if(floor(x)!=x) return 0; //return false  
      }	  
    }

	return 1; //return true
}

/* Find the first variable that is not an integer */
int find_non_integervar(GRBmodel *model){
 	int numvars;	
	double x;
	int error = 0;
	
	GRBenv* env = GRBgetenv(model);
	if (!env) fprintf(stderr, "ERROR: FIND_NON_INTEGRVAR: Failed to get env from Gurobi model\n");
			
    error = GRBgetintattr(model, "NumVars", &numvars);
    if (error) error_handler(env);
	
    for (int j = 0; j < numvars; j++){
	  	error = GRBgetdblattrelement(model, "X", j, &x);
		if (error) error_handler(env);
		if(floor(x)!=x) 
			return j;
      }
	return -1; //error : all variables are integers
}

/* Find the basis index corresponding to var_index,
   where var_index < N (N is total number of variables in the original problem) */
int find_basis_index(GRBmodel *model, int var_index){
 	int numvars,numconstrs;
	int error = 0;
	
	GRBenv* env = GRBgetenv(model);
	if (!env) fprintf(stderr, "ERROR: FIND_BASIS_INDEX: Failed to get env from Gurobi model\n");
			
    error = GRBgetintattr(model, "NumVars", &numvars);
    if (error) error_handler(env);
	
    error = GRBgetintattr(model, "NumConstrs", &numconstrs);
    if (error) error_handler(env);
	int *bhead = (int *) malloc( sizeof(int) * numconstrs); 
    if (!bhead) error_handler(env);	
	error = GRBgetBasisHead (model, bhead);
    if (error) error_handler(env);
	
	int basic_index = -1;
    for (int k = 0; k < numconstrs; k++)
		if (bhead[k] == var_index)
		  basic_index = k;
	free(bhead);
	return basic_index; //error: no corresponding basis found
}


/* Add a CG cut as described in the assignment */
void add_cg_cut(GRBmodel* model){
 	int numvars,numconstrs;	
	int error = 0;
	
	GRBenv* env = GRBgetenv(model);
	if (!env) fprintf(stderr, "ERROR: ADD_CG_CUT: Failed to get env from Gurobi model\n");
	int non_intvar_index = find_non_integervar(model);
	if(non_intvar_index == -1) fprintf(stderr, "ERROR: ADD_CG_CUT: Failed to find a fractional variable\n");
	
	int basis_index = find_basis_index(model,non_intvar_index);
	if(basis_index == -1) fprintf(stderr, "ERROR: ADD_CG_CUT: Failed to find a basis corresponding to fractional variable\n");
	
    error = GRBgetintattr(model, "NumVars", &numvars);
    if (error) error_handler(env);
    error = GRBgetintattr(model, "NumConstrs", &numconstrs);
    if (error) error_handler(env);
	
	int *ind = (int *) malloc( sizeof(int) * numconstrs);
    if (!ind) error_handler(env);
	double *val = (double *) malloc( sizeof(double) * numconstrs);
    if (!val) error_handler(env);

	GRBsvec binv_row = {numconstrs, ind, val};
	double bbsolve_val = 1.0;
	GRBsvec basis_index_vector = {1, &basis_index, &bbsolve_val};

    error = GRBBSolve(model, &basis_index_vector, &binv_row);
    if (error) error_handler(env);
	
	for(int i=0; i<binv_row.len; i++)
		binv_row.val[i] -= floor(binv_row.val[i]);
	/* binv_row is now vector 'u' : which will be used to produce GC cut */
		
	/* get the rhs; that is, the vector 'b' in Ax <= b */
	double *rhs = (double *) malloc( sizeof(double) * numconstrs);
    if (!rhs) error_handler(env);
    error =  GRBgetdblattrarray(model, "RHS", 0, numconstrs, rhs);
	
	/* calculate rhs for the new GC cut */
	double gc_rhs = 0;
	for(int i=0; i<binv_row.len; i++)
		gc_rhs += binv_row.val[i] * rhs[binv_row.ind[i]];
	free(rhs);
	

	/* calculate uA : linear combination of constraints (for GC cut) */
	double *gc_val = (double *) malloc( sizeof(double) * numvars);
    if (!gc_val) error_handler(env);
	for (int i=0; i < numvars; i++){
		gc_val[i] = 0;
		for (int j=0; j < binv_row.len; j++){
			double coeff;
			error = GRBgetcoeff(model, binv_row.ind[j], i, &coeff);
			if (error) error_handler(env);
			gc_val[i] += binv_row.val[j]*coeff;
		}
	}

	int numnz = 0;
	for (int i=0; i < numvars; i++){
		gc_val[i] = floor(gc_val[i]);	
		if(gc_val[i] != 0) numnz++;
	}
	int *gc_cind = (int *) malloc( sizeof(int) * numnz);
    if (!gc_cind) error_handler(env);
	double *gc_cval = (double *) malloc( sizeof(double) * numnz);	
    if (!gc_cval) error_handler(env);
	
	for (int i=0, j=0; i < numvars; i++)
		if(gc_val[i] != 0) {
			gc_cind[j] = i;
			gc_cval[j] = gc_val[i];
			j++;
		}

	/* add GC cut */
	error = GRBaddconstr(model,numnz,gc_cind,gc_cval,GRB_LESS_EQUAL,floor(gc_rhs),NULL);
	if (error) error_handler(env);
	
	
	error = GRBupdatemodel(model);
    if (error) error_handler(env);
	
	/* free heap memory */
	free(ind);
	free(val);
	free(gc_val);
	free(gc_cval);
	free(gc_cind);
}	



int main(int   argc, char *argv[]){	
    if (argc < 2) {
       fprintf(stderr, "Usage: cg_procedure.x filename\n");
       exit(1);
     }
	 
	GRBenv	*env = NULL;
	GRBmodel *model = NULL;
	int error = 0;
	int ismip = 0;
	int optimstatus;
	double objval = -1.0/0.0;
	
	/* read in integer knapsack problem from mps file */
	error = GRBloadenv(&env,"cg.log");
	if(error)	error_handler(env);

    error = GRBreadmodel(env, argv[1], &model);
	if(error)	error_handler(env);
	
    error = GRBgetintattr(model, "IsMIP", &ismip);
	if(error)	error_handler(env);
	
    if (ismip == 0) {
       printf("Model is not a IP\n");
       error_handler(env);
     }
	 
   /* Change objective sense to maximization,
	  because Gurobi default sense is minimization */
    error = GRBsetintattr(model, GRB_INT_ATTR_MODELSENSE, GRB_MAXIMIZE);
    if (error) error_handler(env);
	  
	/* LP relaxation for the IP */
	ip2lp(model);

	error = GRBsetintparam(env, "OutputFlag", 0);
    if (error) error_handler(env);

	/* add GC fractional cuts until optimum integral solution found or infeasible */	
	while(1){
	    /* Solve current formulation */
	    error = GRBoptimize(model);
		if(error)	error_handler(env);
		
		
	    /* Capture solution information */
	    error = GRBgetintattr(model, GRB_INT_ATTR_STATUS, &optimstatus);
	    if (error) error_handler(env);

	    if (optimstatus == GRB_OPTIMAL) {
	      error = GRBgetdblattr(model, GRB_DBL_ATTR_OBJVAL, &objval);
  	  	  if (error) error_handler(env);
	    } else if (optimstatus == GRB_INF_OR_UNBD) {
	      break;
	    } else if (optimstatus == GRB_INFEASIBLE) {
		  break;
	    } else if (optimstatus == GRB_UNBOUNDED) {
  		  break;
	    } else {
	      printf("Optimization was stopped with status = %d\n\n", optimstatus);
	    }
		
		/* check if optimum solution is integral */
		if(is_integral_solution(model))
			break;
		
		/* Current formulation has non-integral solution, so add a CG cut and solve again */
		add_cg_cut(model);
		error = GRBresetmodel(model);
		if (error) error_handler(env);					
    }
	
	/* optimization was unsuccessful, report error and exit */
	if(objval == -1.0/0.0)	error_handler(env);
	
	/* print the results to the file optimal.sol */
	error = GRBwrite(model,"./optimal.sol");
	if (error) error_handler(env);
	
	/* free memory */
	error = GRBfreemodel(model);
	if (error) error_handler(env);	
	GRBfreeenv(env);
	
	return 0;
}